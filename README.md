### What is this project about?
    This is a demonstration how things work in C lang.
    a web service that uses nghttp2, libevent http, cassandra, aerospike, kafka
    

### How to build this project?

### Libraries to build 
    
    apt-get install libevent-dev libnghttp2-dev libjansson-dev

#### How to run this app?
    docker-compose up
    docker  exec -it 63d96adcbb17 /bin/bash
    cd /home/cservice
    cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja
    or 
    bash ./autoBuild.sh
    ./cservice.out
    ./apr_skiplist_test.out
### TO Dos for this project

