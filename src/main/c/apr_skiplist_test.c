
#include "apr_skiplist.h"
#include "apr_file_io.h"
#include "apr_file_info.h"
#include "apr_errno.h"
#include "apr_general.h"
#include "apr_lib.h"
#include "apr_strings.h"
#include "apr_pools.h"
#include "apr_general.h"
#include "apr_hash.h"
#include "apr_lib.h"
#include "apr_time.h"
 #include <openssl/bio.h>
 #include <openssl/evp.h>

struct impression {
    char* name;
    int id;
};

static apr_pool_t *ptmp = NULL;
static apr_pool_t *p;

static int comp(void *a, void *b){
    struct impression* impA = (struct impression*) a;
    struct impression* impB = (struct impression*) b;
    return impA->id - impB->id;
}

int hashTablePlay1 (void) {
    apr_pool_t *pool;
    if (apr_pool_create(&pool, NULL) != APR_SUCCESS) {
        fprintf(stderr, "Something went wrong\n");
        exit(-1);
    }
    fprintf(stdout, "Pool Created\n");

    // apr_table_t * t1 = apr_table_make(pool, 5);
    apr_hash_t* ht = apr_hash_make (pool);

    struct impression *imp = malloc(sizeof(struct impression));
    imp->id = 23;
    imp->name = "reza";
    apr_hash_set (ht, imp->name, strlen(imp->name), imp);
    struct impression* impRetrieved = (struct impression*) apr_hash_get (ht, imp->name, strlen(imp->name)); 
    printf("impRetrieved : id %d with name %s\n", impRetrieved->id, impRetrieved->name);
    printf("hash table size : %d\n", apr_hash_count(ht));
    
}

int skipListPlay1 (void) {
    puts("List is created0");
    apr_skiplist *sl = NULL;
    if (APR_SUCCESS == apr_skiplist_init(&sl, ptmp)) {
        puts("init success");
    }
    
    apr_skiplist_set_compare(sl, comp, comp);

    for (int i = 0; i < 12; i++) {
        struct impression *imp = malloc(sizeof(struct impression));
        imp->id = i;
        imp->name = "reza";
        apr_skiplist_add(sl, imp);
    }
    
    struct impression *findNode = malloc(sizeof(struct impression));
    findNode->id = 2;
    findNode->name = "reza";
    struct impression *result = (struct impression*) apr_skiplist_find(sl, findNode, NULL);
    if (result != NULL) {
        puts(result->name);
        printf("Found: %s\n", result->name);
    }

    apr_skiplistnode *iter = NULL;
    
    // iterate thru skiplist
    apr_skiplistnode *n;
    for (n = apr_skiplist_getlist(sl); n; apr_skiplist_next(sl, &n)) {
        void* data = apr_skiplist_element(n);
        struct impression *result = (struct impression*) (data);
        printf("visiting: %s with ID : %d \n", result->name, result->id);
    }
    
    for (n = apr_skiplist_getlist(sl); n; apr_skiplist_next(sl, &n)) {
        void* data = apr_skiplist_element(n);
        struct impression *result = (struct impression*) (data);
        printf("Freeing : %s with ID : %d \n", result->name, result->id);
        free(result);
    }


    return 0;
}

void base64Encode() {
    BIO *bio, *b64;
    char message[] = "Hello World \n";

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(stdout, BIO_NOCLOSE);
    
    BIO_push(b64, bio);
    BIO_write(b64, message, strlen(message));
    BIO_flush(b64);

    BIO_free_all(b64);
}

void base64Decode() {
    BIO *bio, *b64, *bio_out;
 
    printf("enter 24 character to be decoded, Enter Ctr+D after you are done typing\n");
    //https://gist.github.com/darrenjs/4645f115d10aa4b5cebf57483ec82eca
    char inbuf[512];
    int inlen;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(stdin, BIO_CLOSE);
    bio_out = BIO_new_fp(stdout, BIO_NOCLOSE);
    BIO_push(b64, bio);
    
    inlen = BIO_read(b64, inbuf, 512);
    BIO_write(bio_out, inbuf, inlen);
    printf(" inlen %d", inlen);
    while(inlen > 0) {
        inlen = BIO_read(b64, inbuf, 512);
        printf("writing.....");
        BIO_write(bio_out, inbuf, inlen);
    }
    printf(" flushing.....");

    BIO_flush(bio_out);
    BIO_free_all(b64);
}

int main (void) {
    apr_initialize();

    base64Encode();
    base64Decode();
    // hashTablePlay1();
    // skipListPlay1();
    return 0;
}
